const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ErrorOverlayPlugin = require('error-overlay-webpack-plugin');

module.exports = {
   entry: './src/Index.tsx',
   mode: 'development',
   output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'build'),
      publicPath: '/'
   },
   devServer: {
      contentBase: path.join(__dirname, 'build'),
      compress: true,
      historyApiFallback: true, // передавать index.html после 404 ответов
      disableHostCheck: true,
      hot: true, // включение hot reload
      host: '0.0.0.0',
      port: 3000,
      serveIndex: true,
      proxy: { //ToDo рабочий вариант из контейнеров (сервер должен прослушивать 4000 путь "/" )
         "/api": {
            target: {
               host: "server",
               protocol: 'http:',
               port: 4000
            },
            secure: false,
            changeOrigin: true,
         }
      },
   },
   devtool: 'inline-source-map',
   plugins: [
      new CleanWebpackPlugin({ // https://github.com/johnagan/clean-webpack-plugin#options-and-defaults-optional
         dry: true,
         cleanStaleWebpackAssets: false
      }),
      new HtmlWebpackPlugin({
         template: path.join(__dirname, "src", "index.html")
      }),
      new ForkTsCheckerWebpackPlugin({
         async: false,
         eslint: {
            files: "./src/**/*",
         },
      }),
      new ErrorOverlayPlugin()
   ],
   
   module: {
      rules: [
         {
            test: /\.(js|ts)x?$/,
            exclude: /node_modules/,
            use: {
               loader: 'babel-loader',
            },
         },
      ],
   },
   
   resolve: {
      extensions: [".tsx", ".ts", ".js"],
   },
};