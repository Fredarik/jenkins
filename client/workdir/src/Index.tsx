import * as ReactDom from 'react-dom';
import * as React from 'react';
import TypeScriptComponent from "./TypeScriptComponent/index";

const root = document.getElementById('root');

ReactDom.render(
   <TypeScriptComponent/>,
   root
);